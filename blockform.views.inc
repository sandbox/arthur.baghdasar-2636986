<?php

function blockform_views_data() {
	$data['blockform_db'] = array(
	  'table' => array(
		'base' => array(
		  'field' => 'pid',
		  'title' => 'Blockform Db',
		  'help' => 'The base table for Block Form custom module.',
		),
		'group' => 'Blockform Db',
	  ),
	  'pid' => array(
		'title' => 'Pid',
		'help' => 'Primary Key: Unique person ID.',
		'field' => array(
		  'handler' => 'views_handler_field_numeric',
		  'click sortable' => TRUE,
		),
	  ),
	  'first' => array(
		'title' => 'First Name',
		'help' => 'First name input',
		'field' => array(
		  'handler' => 'views_handler_field',
		  'click sortable' => TRUE,
		),
	  ),
	  'last' => array(
		'title' => 'Last Name',
		'help' => 'Last name input',
		'field' => array(
		  'handler' => 'views_handler_field',
		  'click sortable' => TRUE,
		),
	  ),
	  'unique' => array(
		'title' => 'PageViews',
		'help' => 'Auto increment',
		'field' => array(
		  'handler' => 'views_handler_field_numeric',
		  'click sortable' => TRUE,
		  
		  ),
	  ),
	);
	return $data;
}